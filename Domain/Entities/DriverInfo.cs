﻿using Common.Enums;

namespace Domain.Entities
{
    public class DriverInfo : BaseEntity
    {
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public Genders Gender { get; set; }
        public Driver Driver { get; set; }
        public int DriverId { get; set; }
    }
}
