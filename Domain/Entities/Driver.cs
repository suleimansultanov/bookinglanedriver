﻿namespace Domain.Entities
{
    public class Driver : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DriverInfo DriverInfo { get; set; }
    }
}
