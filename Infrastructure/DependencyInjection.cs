﻿using GraphQL;
using GraphQL.Types;
using Infrastructure.DbContext;
using Infrastructure.DbContext.Repository.Implementations;
using Infrastructure.GraphQL.InputTypes;
using Infrastructure.GraphQL.Mutations;
using Infrastructure.GraphQL.Queries;
using Infrastructure.GraphQL.Schemas;
using Infrastructure.GraphQL.Types;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructureDb(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDbContext>(options =>
                options.UseNpgsql(
                    configuration.GetConnectionString("BookingLaneDriverConnection"),
                    b => b.MigrationsAssembly(typeof(AppDbContext).Assembly.FullName)));
            return services;
        }

        public static IServiceCollection AddGraphQLConfiguration(this IServiceCollection services)
        {
            services.AddSingleton<IDocumentExecuter, DocumentExecuter>();
            services.AddScoped<DriverQuery>();
            services.AddScoped<DriverMutation>();
            services.AddSingleton<DriverType>();
            services.AddSingleton<DriverInputType>();
            services.AddSingleton<DriverInfoType>();   
            
            var sp = services.BuildServiceProvider();
            services.AddSingleton<ISchema>(new BookinglaneSchema(new FuncDependencyResolver(type => sp.GetService(type))));
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<DriverRepository>();
            return services;
        }
    }
}
