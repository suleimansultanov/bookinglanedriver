﻿using GraphQL.Types;
using Infrastructure.DbContext.Repository.Implementations;
using Infrastructure.GraphQL.Types;

namespace Infrastructure.GraphQL.Queries
{
    public class DriverQuery : ObjectGraphType
    {
        public DriverQuery(DriverRepository repo)
        {
            Field<ListGraphType<DriverType>>(
                "drivers",
                resolve: context => {

                    return repo.GetAll();
                });
            Field<DriverType>(
                "driver",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: context => repo.GetById(context.GetArgument<int>("id")));
        }
    }
}
