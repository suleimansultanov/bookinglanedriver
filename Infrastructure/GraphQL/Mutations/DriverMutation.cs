﻿using Common.Models.Dto;
using Domain.Entities;
using GraphQL.Types;
using Infrastructure.DbContext.Repository.Implementations;
using Infrastructure.GraphQL.InputTypes;
using Infrastructure.GraphQL.Types;

namespace Infrastructure.GraphQL.Mutations
{
    public class DriverMutation : ObjectGraphType
    {
        public DriverMutation(DriverRepository repo)
        {
            Name = "CreateDriverMutation";

            Field<DriverType>(
              "createDriver",
              arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<DriverInputType>> { Name = "driver" }
              ),
              resolve: context =>
              {
                  var driverModel = context.GetArgument<DriverDto>("driver");
                  var driver = new Driver
                  {
                      FirstName = driverModel.FirstName,
                      LastName = driverModel.LastName,
                      DriverInfo = new DriverInfo { PhoneNumber = driverModel.PhoneNumber, Address = driverModel.Address }
                  };
                  var newDriver = repo.Create(driver);
                  repo.SaveAsync().GetAwaiter();
                  return newDriver;
              });
        }
    }
}
