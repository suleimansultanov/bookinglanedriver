﻿using Newtonsoft.Json.Linq;

namespace Infrastructure.GraphQL.QueryModels
{
    public class GraphqlQuery
    {
        public string OperationName { get; set; }
        public string Query { get; set; }
        public JObject Variables { get; set; }
    }
}
