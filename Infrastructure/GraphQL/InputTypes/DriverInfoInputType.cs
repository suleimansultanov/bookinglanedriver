﻿using GraphQL.Types;

namespace Infrastructure.GraphQL.InputTypes
{
    public class DriverInfoInputType : InputObjectGraphType
    {
        public DriverInfoInputType()
        {
            Name = "DriverInfoInput";
            Field<StringGraphType>("phoneNumber");
            Field<StringGraphType>("address");
        }
    }
}
