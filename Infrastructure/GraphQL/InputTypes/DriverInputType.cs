﻿using GraphQL.Types;

namespace Infrastructure.GraphQL.InputTypes
{
    public class DriverInputType : InputObjectGraphType
    {
        public DriverInputType()
        {
            Name = "DriverInput";
            Field<StringGraphType>("firstName");
            Field<StringGraphType>("lastName");
            Field<StringGraphType>("phoneNumber");
            Field<StringGraphType>("address");
        }
    }
}
