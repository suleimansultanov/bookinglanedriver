﻿using Common.Enums;
using GraphQL.Types;

namespace Infrastructure.GraphQL.InputTypes.EnumTypes
{
    public class GenderEnumType : EnumerationGraphType<Genders>
    {
    }
}
