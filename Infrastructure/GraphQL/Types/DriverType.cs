﻿using Domain.Entities;
using GraphQL.Types;
using System.Collections.Generic;

namespace Infrastructure.GraphQL.Types
{
    public class DriverType : ObjectGraphType<Driver>
    {
        public DriverType()
        {
            Field(x => x.Id);
            Field(x => x.FirstName);
            Field(x => x.LastName);
            Field<ListGraphType<DriverInfoType>>(
                "driverInfo",
                resolve: context => {
                    List<DriverInfo> driverInfos = new();
                    driverInfos.Add(context.Source.DriverInfo);
                    return driverInfos;
                });
        }
    }
}
