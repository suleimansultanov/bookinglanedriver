﻿using Domain.Entities;
using GraphQL.Types;

namespace Infrastructure.GraphQL.Types
{
    public class DriverInfoType : ObjectGraphType<DriverInfo>
    {
        public DriverInfoType()
        {
            Field(x => x.Id);
            Field(x => x.PhoneNumber);
            Field(x => x.Address);
            Field(x => x.Gender);
        }
    }
}
