﻿using GraphQL;
using GraphQL.Types;
using Infrastructure.GraphQL.Mutations;
using Infrastructure.GraphQL.Queries;

namespace Infrastructure.GraphQL.Schemas
{
    public class BookinglaneSchema : Schema
    {
        public BookinglaneSchema(IDependencyResolver resolver) : base(resolver)
        {
            Query = resolver.Resolve<DriverQuery>();
            Mutation = resolver.Resolve<DriverMutation>();
        }
    }
}
