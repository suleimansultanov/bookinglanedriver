﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.DbContext.Repository.Interfaces
{
    public interface IRepository<T> : IDisposable
        where T : class
    {
        IEnumerable<T> GetAll(); // получение всех объектов
        T GetById(int id); // получение одного объекта по id
        Task<T> Create(T item); // создание объекта
        void Update(T item); // обновление объекта
        void Delete(T entity); // удаление объекта по id
        Task SaveAsync();  // сохранение изменений
    }
}
