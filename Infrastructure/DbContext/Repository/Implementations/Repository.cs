﻿using Infrastructure.DbContext.Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.DbContext.Repository.Implementations
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly AppDbContext Context;

        public Repository(AppDbContext context)
        {
            Context = context;
        }

        public virtual TEntity GetById(int id) 
        {
            return Context.Find<TEntity>(id);
        }

        public virtual async Task<TEntity> Create(TEntity entity)
        {
            await Context.Set<TEntity>().AddAsync(entity);
            return entity;
        }
        public async Task SaveAsync()
        {
            await Context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Context.Dispose();
        }

        public virtual void Update(TEntity entity)
        {
            Context.Set<TEntity>().Update(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return Context.Set<TEntity>();
        }
    }
}
