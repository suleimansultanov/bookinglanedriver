﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Infrastructure.DbContext.Repository.Implementations
{
    public class DriverRepository : Repository<Driver>
    {
        public DriverRepository(AppDbContext context) : base(context) { }

        public override IEnumerable<Driver> GetAll()
        {
            return Context.Drivers.Include(d => d.DriverInfo);
        }
    }
}
