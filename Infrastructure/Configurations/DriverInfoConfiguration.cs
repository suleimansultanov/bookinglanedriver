﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Configurations
{
    public class DriverInfoConfiguration : IEntityTypeConfiguration<DriverInfo>
    {
        public void Configure(EntityTypeBuilder<DriverInfo> builder)
        {
            builder.ToTable("driver_infos");
            builder.HasKey(k => k.Id);
            builder.HasOne(d => d.Driver).WithOne(d => d.DriverInfo).HasForeignKey<DriverInfo>(d => d.DriverId);
        }
    }
}
