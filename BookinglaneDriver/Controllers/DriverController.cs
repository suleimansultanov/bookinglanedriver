﻿using Infrastructure.DbContext;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookinglaneDriver.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DriverController : ControllerBase
    {
        private readonly AppDbContext _context;
        public DriverController(AppDbContext context)
        {
            _context = context;
        }
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_context.Drivers);
        }
    }
}
