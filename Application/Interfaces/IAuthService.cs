﻿using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IAuthService
    {
        Task AuthAsync();
    }
}
